//?async and await
/*
const callApi = async () => {
    try {
        let fetchApi = await fetch("https://catfact.ninja/fact/")
        let data = await fetchApi.json()
//         console.log(`The fact  :- ${data.fact} 
// The length  "${data.length}"`)
        return data
    } catch (error) {
        console.log("Error", error)
    }
}
callApi().then((data) => {
    if (data) {
        console.log(`The fact  :- ${data.fact} 
The length  "${data.length}"`)
        console.log("----  Another Data ----")
        callApi().then((Newdata) => {
           // let Newdata=true
           
            if (Newdata) {
                console.log(`The fact  :- ${Newdata.fact} 
The length  "${Newdata.length}"`)
            }
        }).catch((err) => {
            console.log("Error innner ", err)
        })
    }
   
}).catch(err => {
    console.log("Error by outside ", err)
})
*/
/*
const callApi = async () => {
    try {
        let fetchApi = await fetch("https://catfact.ninja/fact/");
        let data = await fetchApi.json();
//         console.log(`The fact :- ${data.fact} 
// The length :- "${data.length}"`);
return data;
    
    } catch (error) {
        console.log("Error", error);
    }
};


callApi()
.then((data)=>{
    console.log(`fact ${data.fact}  and length "${data.length}"`)
    console.log("---- Another Data ----")
    return callApi()
})
.then((data)=>{
    console.log(`fact ${data.fact}  and length "${data.length}"`)
}
).catch(err=>{
    console.log(err,"Error outside")
})

*/




let stock = {
    Fruite: ["mango", "banana", "orange", "coco"],
    liquid: ["water", "ice"],
    holder: ["cup", 'cone', "stick"],
    topping: ["chocolate", "cherry", "dry-fruits"]
}
let shop = true
let order = (work) => {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res(console.log("--Start--"))
        }, 2000)
    })
}




async function work() {
    console.log("Take the order")
    console.log("Order given to the kitchen")
    await order()
    console.log(`prepare the order and serve to the table`)

    console.log("--End--")
}


//work()
//console.log("Other order taken")

//this in object

const user1={
    name:"Nikhil",
    printName:function(){
        console.log(this.name)
    } 
}


const user2={
    name:"Lucky"
} 

//user1.printName()

// If want to print the user2 name then
//user1.printName.call(user2)


 let num=1234
//  let num1 = num.toString()

//  let ans =""
//  for (let i = num1.length -1; i >= 0; i--) {
//    ans+= num1[i]
//  }
// console.log(parseInt(ans))

// let ans =0
// while(num){
//     let digit= num%10
//     ans = ans*10+digit
//     num=Math.floor(num/10)
// }
// console.log(ans)
