// "?" this show the topics names

var b = function add(name){
    console.log("Adding the number",name);
}
//b("G") //*  it is  not for the "add()" error show 
//add("q")//add is not defined

//// ?function statement and function declaration are same 
// fun1("statement")
// function fun1(name){
//     console.log(name)
// }

// //? function expression
// let a =function(name){
//     console.log(name);
// }
// a("expression")


// //?First class function
/*
let x= function(fun1){
    
  console.log("fun1")
  return ((a,b)=>{
    
        console.log("Inside the mult");
        return (a*b)
        
  })(10,20)
}
console.log(
    x(((a,b)=>{
        console.log(a+b);
    })(10,20))
)
*/
// //? higher order function
/*
function msg0(name){
    return `hello ${name}`
}

function msg1(start,mid,end,name){
    console.log( `${start(name)}  Middle: ${mid} end: ${end}`);

}

msg1(msg0,"kumar","varma","nikhil")
*/

// //? scope
/*  var a=50
function fun1(){
    var a=10
    fun2()
    function fun2(){
        var a=20
        console.log("fun2 g",a)
    }
    console.log("fun1 gol",a)
}
console.log("google",a);

fun1();

*/

// //?this
//console.log(this) // empty object in terminal but window object in chorme

// function ha(){
//     console.log(this)
// }

// ha()

// //!  falsy value : NaN,null,undefined,false,0,-0,BigInt 0n,""
////!   truthy value: "0",'false'," ",[],{},function(){}


// //? checking the object is empty or not

const obj ={}

//if(Object.keys(obj).length === 0) console.log("Empty Object")


// Nullish coaleshing operate (??)
 let val1
//val1 = null ?? 10
//val1 = undefined ?? 20
//console.log(val1)

// //?Event loop  

//  Microtask queue > Callback queue

console.log("Start");
setTimeout(function (){
    console.log("setTime out")
},1000)


// It will execute console.log("fetch the data from the api") immediately when the then method is called, rather than waiting for the fetch operation to complete. 
fetch("https://fakestoreapi.com/products/")
.then(
    console.log("fetech the data from the api")
)
console.log("End");

/*
console.log("--Start--");
setTimeout(function one(){
    console.log("setout")
},2000)

fetch("https://fakestoreapi.com/products/")
.then(
//   ()=>{
//         console.log("api arrow");
//     }
function(){
    console.log("api function exp")
}
//console.log("apiii")
)
console.log("--End--");
*/


// function add(...number){
//     let sum=0
//   for (let ite of number) {
//     sum+=ite
//   }
//   return sum
// }

// console.log(add(1,2,3))

// var j=0
// for(j=0;j<5;j++){
//    setTimeout(()=> console.log(j),0)
// }
// console.log(j +" outre");



