
//this keyword

const obj12 = {
    email: "abc@gamil.com",
    update (email){
        this.email =email

    }

}

//console.log(obj12.email)
obj12.update("xyz@gamil.com")
//console.log(obj12.email)

function x(){
    console.log(this)
}
//x()
//window.x()

const obj1 = {
    js: "javascript",
    java: "java",
    sd: 'solidity',
    py: 'python'
}

for (const iter in obj1) {
    //console.log(`key  is ${iter} and  value is ${obj1[iter]}`)
}

const arr = [1, 2, 3, 4, 5, 6]

arr.forEach((number) => {
     // console.log(number*10);
})

//console.log(arr);

const db = [
    {
        lang: 'JS',
        top: 1
    }, {
        lang: 'java',
        top: 3
    }, {
        lang: 'rust',
        top: 4
    }, {
        lang: 'cpp',
        top: 2
    }
]

db.forEach((oneobj) => {
    // console.log(oneobj["top"]);
    for (const key in oneobj) {
       //   console.log(`key is ${key} and value ${oneobj[key]}`)
    }
})


const javaReturn = db.filter((one) => one["top"] === 1)

//console.log(javaReturn);

//javaReturn.forEach((item )=> console.log(item.lang))

// const arr1 = [1, 2, 3, 4]

// const reduceArr = arr1.reduce((acc, curval) => {
//      console.log(acc ,curval)
//     return acc + curval
// }, 10)
// console.log(reduceArr);



// function abc(){
//     let name = "abcd"
//   const ans= ()=>{
// console.log(name)
//  }
// ans()
// }
// abc()


/////////////////////////////////////
//*Learn Callbacks, Promises  By Making Ice Cream
let stock = {
    Fruite: ["mango", "banana", "orange", "coco"],
    liquid: ["water", "ice"],
    holder: ["cup", 'cone', "stick"],
    topping: ["chocolate", "cherry", "dry-fruits"]
}

/*
let order = (Fruits_name, call_making) => {
    setTimeout(() => {
        console.log(`${stock.Fruite[Fruits_name]} is selected for the ice-cream`)
        call_making()
    }, 2000)


}
let making = () => {
    setTimeout(() => {
        console.log(`making of ice-cream is started `)
        setTimeout(() => {
            console.log(`fruite is chopped`)
            setTimeout(() => {
                console.log(`${stock.liquid[0]} & ${stock.liquid[1]} is added`)
                setTimeout(() => {
                    console.log("machine is Started")
                    setTimeout(() => {
                        console.log(`Container is selected ${stock.holder[0]}`)
                        setTimeout(() => {
                            console.log(`Topping is selected ${stock.topping[0]}`)
                            setTimeout(() => {
                                console.log("Ice cream is served")
                            }, 2000)
                        }, 3000)
                    }, 2000)
                }, 1000)
            }, 1000)
        }, 2000);
    }, 4000)
}

order(3, making)
*/


let shop_open = true

let order = (work) => {
    return new Promise((res, rej) => {
        if (shop_open) {
            setTimeout(() => {
                res(work())
            }, 1000);
        } else {
            rej(console.log("Shop is false"))
        }
    }
    )
}


order(() => console.log(`${stock.Fruite[2]} fruit selected`))
    .then(
        () => {
            return order(() => console.log(`making of ice cream is started`))
        }
    )
    .then(
        () => {
            return order(() => console.log(` ${stock.holder[1]} holding is selected`))
        }
    )
    .then(
        () => {
            return order(() => console.log(`topping :- ${stock.topping[1]}  done`))
        }
    )
    .then(
        () => {
            return order(() => console.log(`icream is served`))
        }
    )
    .catch((err)=> console.log("Error ",err) )
.finally(()=> console.log(`Next Order
  If error show then try again`))












