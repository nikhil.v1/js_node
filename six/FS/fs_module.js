
const fs = require('fs')

//? Read and Create CMD

fs.writeFileSync("REPL.txt","REPL stand for  Read-Eval-Print-Loop")

//Async
function create(){
    fs.writeFile("REPL.txt","REPL stand for  Read-Eval-Print-Loop",function (err){
        if(err){
            console.log("Error",err)
        }else{
            console.log("Created")
        }
    })
}
//create()

//console.log("Next Step")


// let data = fs.readFileSync('REPL.txt',"utf8")
// console.log(data)
//console.log(fs.readFileSync('REPL.txt').toString())


//console.log("Read the file")
let data1=()=>{
    fs.readFile("REPL.txt",(err,data)=>{
        if(err) console.log("Error", err)
            else console.log(`File Data :- ${data}`)
    })
}
//data1()


// Delete the file
/*
try {
    fs.unlinkSync('REPL.txt')
    console.log("Deleting the file")
} catch (error) {
    console.log("Error",error)
}
console.log("File deleted")
*/
/*
fs.appendFile("REPL.txt",". \nIt is useful in writing and debugging the codes.",(err)=>{
    if(err){
        console.log("Ërror found",err)
        return
    }
    console.log("Text is added successfully")
})

console.log("Adding the data")
*/
// Rename the file

function renameFileSync(){
    fs.renameSync("REPL.txt","repl.txt")
    console.log("File renamed")
}

//renameFileSync()

let renameFileAsyns=()=>{
    fs.rename("repl.txt","REPL.txt",(err)=>{
       if(err){
        console.log("Error",err)
        return
       }
    })
 
    console.log("Renamed Succsefully")
}
//renameFileAsyns()

// var a=100;
// (function(){
//    let a=10
//     console.log(a)
// })()

// console.log(a)