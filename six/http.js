/*
Small project of product the data of product is on data.json and by using "http" url- "localhost:8080/products"  get all product data in json formate 
And to get the individual product id(1-10) data the  url- "localhost:8080/products/id" for eg "localhost:8080/products/5" so id=5 data will be there 
*/


const http = require('http')
const url = require('url')
const fs = require('fs')
const { json } = require('stream/consumers')
const port = 8080
const data = fs.readFileSync("./data.json", 'utf8')
const productData = JSON.parse(data)



const server = http.createServer((req, res) => {
    const pathURL = req.url.split('/');

    // console.log("URL ", pathURL)
    if (pathURL[1] === "" || pathURL[1] === undefined) {
        res.end("Welcome to the Server")
    } else if (pathURL[2] !== undefined && !isNaN(pathURL[2] && pathURL[1] === "products")) {
        // dynimacally url 
        const number = Number(pathURL[2])
        const oneData = productData.find(elment => {
            return elment.id === number
        });
        // console.log(number)
        // console.log("OneData",oneData)
        // res.writeHead(200, { 'Content-type': 'application/json' })
        // res.end(JSON.stringify(oneData))
        if (oneData) {
            // console.log("OneData", oneData);
            res.writeHead(200, { 'Content-type': 'application/json' });
            res.end(JSON.stringify(oneData));
        } else {
            res.writeHead(404, { "Content-type": "text/html" });
            res.end("<h1>404 Not Found</h1>");
        }
    } else if (pathURL[1] === "products") {
        res.writeHead(200, { 'Content-type': 'application/json' })
        res.end(data)
    }
    else {
        res.writeHead(404, { "Content-type": "text/html" })
        res.end("<h1>404 Not Found</h1>")
    }
})


server.listen(port, () => {
    console.log("===========Server is Started=================")
})


