
// ?data type conversion
/*
let a= "10"

console.log(typeof a)
let aNumber=Number(a)
console.log(typeof aNumber)

let emptyString=""  // false
console.log(typeof emptyString)
let emptyStringBool = Boolean(emptyString)
console.log(emptyStringBool)
let ab = "10abc"
let abType=Number(ab) // NaN
console.log(abType)
*/
//? stack and heap memory
//* stack primitive data type
/*
let one = "one"
let two =one
console.log(one , two)
two="two"
console.log(one,two +" after change")
*/

//* heap non primitive datatype
/*
const user1={
    name:"abc",
    email:"abc@gmail.com",
    printf : function(){
        console.log(this.name)
    }
}

//console.log(user1)
const user2 = user1
//console.log(user2)
user1.name="xyz"
//console.log(user2)
user2.printf()
*/

//?methods 
//let e='Nikhilkumar-Varma'
//console.log(e.split("",6)) //! 6 => is the size  
//console.log(e.replace("-"," "))
//console.log(e.slice(2,4)) //! 4=> is not include
//console.log(e.substring(0,5))  //! 5=> is not include

// function 
//pure amd impure 
/*
function mult(a,b){
    return a*b
}
console.log(mult(2,5))
let y=10;
function incer(){
    return y++
}
console.log(incer())
*/
//Immediately Invoked Functions Expressions
// (function (value){
//     console.log(value)
// })("Nikhil");

// ((a,b)=>{
//   console.log(a*b)
// })(10,2)

//var a=10
// global scope ka pollution k liya   
// for immediately function "";" has to there in the last just like the other 
// (function(x){
//     console.log(x)
// })(10);


// javascript-function-generator

function* simpleIdGenerte(){
 let id=1
 while(true){
    yield id
    ++id
 }
}
const objGenetor=simpleIdGenerte()
const objGenetor1=simpleIdGenerte()
console.log(objGenetor.next())
console.log(objGenetor.next())
console.log(objGenetor.next())
console.log(objGenetor.next())
console.log(objGenetor1.next())
console.log(objGenetor1.next())

// let mySym = Symbol("mykey1")
// const obj={
//     name:"ABCD",
//     [mySym]:"mykey1",
//     "company": "Nimap Infotech"
// }

// console.log(obj.name,obj.company,obj[mySym])


// singleton obj
//const user1={}
// non singleton 
//const user2=new Object()
//* merge the object
/*
const obj1={a:1,b:2}
const obj2={c:3,d:4}

const mainObj=Object.assign({},obj1,obj2)    //? (target,source)
console.log(mainObj);
*/

//Array Destructuring
/*
let animals=["Lion","Tiger","Wolf"]
let [a,b,c]=animals
console.log(a)

let userDetails={
    name:"ADC",
    emial:"adc@gamil.com",
    "company":"Google"
}

let {name,emial,company}=userDetails
console.log(company)

*/

//? spread and rest

//let a=[1,2,3,4,5]
//let b=[6,7,8,9,10]
/*
function sum(...num){
 return [...num]
}
console.log(sum(a,b));
//=> [ [ 1, 2, 3, 4, 5 ], [ 6, 7, 8, 9, 10 ] ]

function sum(...num){
    return [...num]
   }
   console.log(sum(...a,...b));
   //[
 // 1, 2, 3, 4,  5,
//  6, 7, 8, 9, 10
//]
*/
//let [x,y,...z]=a
//console.log(...z);
/*
let u=10
one()
function one(){
    let v=10
    
    function two(){
        let w=10
        three()
        function three(){
            console.log(u+v+w);
        }
    }
    two()
}
*/
let arr=[1,2,3,4]
//arr.push(10,20) //? add he element from back
//arr.pop() //? delete the element from back
//arr.unshift(44,22,33) //? add element from the front 
//arr.shift() //? delete the element from the front 

//console.log(arr)

