//reverse the sttring
let str = "Apple";
let ans = "";
function reverse(str, ans) {
  for (let index = str.length - 1; index >= 0; index--) {
    ans += str[index];
  }
  return ans;
}

function swap(l, r, ans) {
  let t = ans[l];
  ans[l] = ans[r];
  ans[r] = t;
}
let l = 0,
  r = str.length - 1;
while (l < r) {
  swap(l, r, ans);
  l++;
  r--;
}
// console.log(ans)
//console.log(reverse(str,ans))

//palindrom
let str1 = "cddc";
function palindrom(str) {
  let lp = 0;
  let rp = str.length - 1;
  while (lp <= rp) {
    if (str1[lp] !== str[rp]) {
      return false;
    }
    lp++;
    rp--;
  }

  return true;
}

const result = palindrom(str1) ? "Palidrom" : "Not Palidrom ";
//console.log(result);

// capitalize the first letter of every word in the sentence
let str2 = "hello nikhil kumar varma";
let ans1 = "";
//1. method
function strCaptilize(str2) {
  let arrStr = str2.split(" ");
  for (let arr of arrStr) {
    for (let i = 0; i < arr.length; i++) {
      if (i == 0) {
        ans1 += String.fromCharCode(arr.charCodeAt(i) - 32);
        //  ans1+=arr[i].toUpperCase()
      } else {
        ans1 += arr[i];
      }
    }
    ans1 += " ";
  }
}
//strCaptilize(str2)
//console.log(ans1)
// 2. method
// const strCaptilize =(str)=>{
//     return str.split(" ").map(word => word[0].toUpperCase()+ word.slice(1)).join(" ")
// }
//console.log(strCaptilize(str2))

// buy and sell stock

const prices = [7, 1, 5, 3, 6, 4];
let buyPrice = Number.MAX_VALUE;
var maxProfit = 0;

for (let stock of prices) {
  if (buyPrice <= stock) {
    let profit = stock - buyPrice;
    maxProfit = Math.max(maxProfit, profit);
  } else {
    buyPrice = stock;
  }
  //return maxProfit
}

//console.log(maxProfit)

// chunk of arr
let arr = [1, 2, 3, 4, 5, 6, 7];
let size = 3;
let newArr = [];

function chunkArr(arr, n, newArr) {
  let tempArr = [];
  let cnt = 0;
  for (let i = 0; i < arr.length; i++) {
    tempArr.push(arr[i]);
    cnt++;
    if (cnt == n || i == arr.length - 1) {
      newArr.push(tempArr);
      cnt = 0;
      tempArr = [];
    }
  }
}
//chunkArr(arr, size, newArr);
//console.log(newArr)

//two sums

const nums = [2, 7, 11, 15];
let target = 9;

function twoSums(nums, target) {
  for (let i = 0; i < nums.length; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] + nums[j] == target) {
        return { i, j };
      }
    }
  }
  return false;
}
//console.log(twoSums(nums,target))

//pattern
let n = 6;
/*
for (let i = 1; i < n; i++) {
  let line = '';
  for (let j = 1; j <= i; j++) {
      line += '*';
  }
  console.log(line);
}


for (let i = 1; i < n; i++) {
  let line = '';
  for (let j = 1; j <= i; j++) {
      line += `${i}`
  }
  console.log(parseInt(line));
}
  */

let str3 = "abbb";
let str4 = "abnbbb";

// console.log(anagram(str3, str4));

function anagram(str3, str4) {
  let m1 = new Map();
  for (const ele of str3.split("")) {
    if (m1.has(ele)) {
      m1.set(ele, m1.get(ele) + 1);
    } else {
      m1.set(ele, 1);
    }
  }
  //console.log(m1)
  for (const ele of str4.split("")) {
    if (m1.get(ele) === 0) {
      m1.set(ele, m1.get(ele) - 1);
    } else if (m1.has(ele)) {
      m1.delete(ele);
    } else {
      m1.set(ele, 1);
    }
  }

  // console.log(m1.size);
  if (m1.size) {
    return false;
  } else {
    return true;
  }
}

// largest number
let arr1 = [12, 35, 1, 10, 34, 1, 35];

let first = 0,
  second = 0;

for (let i = 0; i < arr1.length; i++) {
  if (first < arr1[i]) {
    first = arr1[i];
  } else if (first > arr1[i] && second < arr1[i]) {
    second = arr1[i];
  }
}
//console.log(first," ",second)

//swap  number

let x = 10;
let y = 20;

x = x + y;
y = x - y;
x = x - y;

//console.log(`x: ${x} and y:${y} `)

//reverse string
let name1 = "abcdefghi";
let rans = "";

for (let i = 0; i < name1.length; i++) {
  rans += name1[i];
}

//console.log(rans)

// let senc="the mirzapur season three"
// let senc1=senc.split(" ")
// let anssec=""
// for (const arr of senc1) {
//   for(let i=0;i<arr.length;i++){
//     if(i == 0){
//       anssec+=arr[i].toUpperCase()
//     }else{
//       anssec+=arr[i]}
//   }
//   anssec+=" "
// }
// console.log(anssec)

let holearr = [1, 2, 3, 4, 5, 6, 7, 8];

function divide(holearr) {
  let arrdiv1 = [];
  let arrdiv2 = [];
  let count = new Map();
  for (const ele of holearr) {
    if (count.has(ele)) {
      count.set(ele, count.get(ele) + 1);
    } else {
      count.set(ele, 1);
    }
  }
  for (let i = 0; i < size; i++) {
    if (count.get(holearr[i]) == 1) {
      arrdiv1.push(holearr[i]);

      count.delete(holearr[i]);
    } else if (count.get(holearr[i]) == 2) {
      arrdiv1.push(holearr[i]);
      arrdiv2.push(holearr[i]);
      count.delete(holearr[i]);
    } else {
      console.log("Cant divide");
    }
  }

  count.forEach((value, key) => {
    if (arrdiv1.length == size + 1) {
      return;
    }
    if (value == 2) {
      arrdiv1.push(key);
      arrdiv2.push(key);

      count.delete(key);
    } else if (value == 1) {
      arrdiv1.push(key);
      count.delete(key);
    }
  });

  count.forEach((value, key) => {
    arrdiv2.push(key);
    count.delete(key);
  });

  if (!count.size) {
    console.log(arrdiv1);
    console.log(arrdiv2);
  }
}
divide(holearr);
