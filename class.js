//  # symbol : private
class Animal{
    constructor (name,leg,id){
        console.log('Animal Call')
        this.id=id
        this.leg=leg
        this.name=name
    }
    printInfo(){
        console.log(`${this.name} , ${this.id} , leg :${this.leg}`)
    }

    changeleg(leg){
        return this.leg =leg
    }

}

// const a1= new Animal("dog",2,"123efb");
// a1.printInfo()
// a1.changeleg(4)
// a1.printInfo()
// const a2= new Animal("cat",3,"123plm");
// a2.printInfo()
// a2.changeleg(4)
// a2.printInfo()



class waterAnimal extends Animal{
    constructor(name,Swim,eat){
        console.log("waterAnimal Call")
        super(name)
        this.eat=eat
        this.Swim=Swim
        
    }
    printInfo(){
        console.log(`${this.name} , ${this.Swim} , eat :${this.eat}`)
    }
}

// const w1=new waterAnimal("coco","yes","nonveg")
// w1.printInfo()

